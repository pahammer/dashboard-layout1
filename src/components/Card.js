// components/Card.js

function SmallCard({ children, title }) {
	return (
		<>
			<div className="bg-white h-52 w-64 rounded-lg shadow-md transition ease-in-out delay-50 hover:-translate-y-1 hover:scale-105 duration-100">
				<div className="p-2">
					<SmallCardTitle title={title} />
					<div>{children}</div>
				</div>
			</div>
		</>
	);
}

function MediumCard({ children, title }) {
	return (
		<>
			<div className="bg-white h-52 md:w-96 lg:96 w-64 sm:col-span-1 md:col-span-2 lg:col-span-2 rounded-xl shadow-sm transition ease-in-out delay-50 hover:-translate-y-1 hover:scale-105 duration-100">
				<div className="p-2">
					<SmallCardTitle title={title} />
					<div>{children}</div>
				</div>
			</div>
		</>
	);
}

function SmallCardTitle({ title }) {
	return <p className="font-bold text-gray-300 text-sm lowercase">{title}</p>;
}

export { SmallCard, MediumCard };
