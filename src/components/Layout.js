// components/Layout.js

import SideBar from "./Sidebar";

export default function Layout({ children }) {
	return (
		<div className="insex-0 flex flex-row bg-gray-100 min-h-screen">
			<SideBar />
			<div className="p-4 flex grow">{children}</div>
		</div>
	);
}
