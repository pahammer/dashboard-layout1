// components/sidebar.js

export default function SideBar() {
	return (
		<>
			<Mobile />
			<div className="hidden lg:block lg:w-44 md:block md:w-36 sm:block sm:w-36 flex-none">
				<div className="flex flex-col h-full pt-12 px-2">
					<SideBarSectionTitle>Pages</SideBarSectionTitle>
					<SideBarLink>Main</SideBarLink>
					<SideBarLink>Analytics</SideBarLink>
					<SideBarSectionTitle>Components</SideBarSectionTitle>
					<SideBarLink>Charts </SideBarLink>
					<SideBarLink>Tables</SideBarLink>
				</div>
			</div>
		</>
	);
}

function SideBarSectionTitle({ children }) {
	return (
		<div className="font-sans py-2 pl-6 text-gray-400 text-sm font-light">
			{children}
		</div>
	);
}

function SideBarLink({ children }) {
	return (
		<div className="font-sans py-2 pl-6 rounded-r-sm rounded-l-lg border-r-4 border-zinc-100 hover:border-purple-500 font-light cursor-pointer hover:bg-white text-gray-700 hover:text-gray-800">
			{children}
		</div>
	);
}

// tbc
// an entirely new nav bar should be rendered when the icon is selected
function Mobile() {
	return (
		<div className="block sm:hidden md:hidden lg:hidden xl:hidden 2xl:hidden">
			<svg
				xmlns="http://www.w3.org/2000/svg"
				fill="none"
				viewBox="0 0 24 24"
				strokeWidth={1.5}
				stroke="currentColor"
				className="w-6 h-6"
			>
				<path
					strokeLinecap="round"
					strokeLinejoin="round"
					d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
				/>
			</svg>
		</div>
	);
}
