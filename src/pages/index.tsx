// components
import { SmallCard, MediumCard } from "../components/Card";

export default function Home() {
	return (
		<div className="flex flex-col w-full h-full mx-auto pt-4 sm:justify-start justify-center items-start space-y-2">
			<div className=" flex flex-row p-3 w-full text-gray-300 rounded-lg items-center">
				Analytics
			</div>
			<div className="flex">
				<div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5">
					<SmallCard title="Title 1">body</SmallCard>
					<SmallCard title="Title 2">body</SmallCard>
					<SmallCard title="Title 3">body</SmallCard>
					<MediumCard title="Title 4">body</MediumCard>
					<MediumCard title="Title 5">body</MediumCard>
				</div>
			</div>
		</div>
	);
}
